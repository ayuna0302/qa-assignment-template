import re
import time
from behave import given, when, then, step  # pylint: disable=no-name-in-module

@given(u'страница "{url}" загружена')
def step_impl(context, url):
    context.driver.get(url)

@given(u'задача (#{id}) выполнена')
def step_impl(context, id):
    input = find_input(context.driver, id)
    if not is_checked(input):
        click(input)
        time.sleep(5)
        context.driver.refresh()

@given(u'задача (#{id}) не выполнена')
def step_impl(context, id):
    input = find_input(context.driver, id)
    if is_checked(input):
        click(input)
        time.sleep(5)
        context.driver.refresh()

@when(u'отмечается чек-бокс задачи (#{id})')
def step_impl(context, id):
    input = find_input(context.driver, id)
    click(input)
    time.sleep(5)
    context.driver.refresh()

@then(u'чек-бокс напротив задачи (#{id}) отмечен')
def step_impl(context, id):
    input = find_input(context.driver, id)
    assert is_checked(input)

@then(u'чек-бокс напротив задачи (#{id}) не отмечен')
def step_impl(context, id):
    input = find_input(context.driver, id)
    assert not is_checked(input)

@then(u'текст задачи (#{id}) отформатирован зачеркнутым')
def step_impl(context, id):
    label = context.driver.find_element_by_css_selector('label[for="%s"]' % id)
    assert re.match(r'line-through', label.value_of_css_property('text-decoration'))

@then(u'текст задачи (#{id}) отформатирован не зачеркнутым')
def step_impl(context, id):
    label = context.driver.find_element_by_css_selector('label[for="%s"]' % id)
    assert not re.match(r'line-through', label.value_of_css_property('text-decoration'))

def find_input(driver, id):
    input = driver.find_element_by_id(id)
    assert input.tag_name == 'input'
    return input

def click(input):
    input.find_element_by_xpath('./..').find_element_by_class_name('iCheck-helper').click()

def is_checked(input):
    return input.get_attribute('value') == 'true'